class Quiz {
  defaultOptions = {
    defaultTemplate: true,
    /** @param {String} containerSel */
    containerSel: '[data-quiz-inline]',
    /** @param {boolean} defaultTemplate - Для выбора варианта кастомного шаблона или дефолтного */
    questionSel: '[data-quiz-question]',
    /** @param {string} questionSel - Селектор для вопросов */
    questionNameAttr: 'data-quiz-question-name',
    /** @param {string} questionSel - Атрибут для имени вопроса */
    questionNameSel: '[data-quiz-question-name]',
    /** @param {string} questionSel - Селектор для имени вопроса */
    answerAttr: 'data-quiz-answer',
    /** @param {string} answerAttr - Атрибут для ответов */
    answerSel: '[data-quiz-answer]',
    /** @param {string} answerSel - Селектор для ответов. Сам атрибут должен быть таким же как и answerAttr */
    answerDependentAttr: 'data-quiz-dependent',
    /** @param {string} answerDependentAttr - Атрибут для ответов, с которого считываются зависимые ответы для следующего шага */
    answersContainerSel: '[data-quiz-answers-container]',
    /** @param {string} answersContainerSel - Селектор для ответов. Сгенерированные ответы вставляются внутрь этого элемента */
    backButtonSel: '[data-quiz-back]', /** @param {string} backButtonSel - Селектор для кнопки «назад» */
    /** @param {string} customLabelReplace - строка для замены атрибутов в custom label */
    customLabelReplace: '{data-template-quiz-label}',
    /** @param {string} customLabelTextReplace - строка для замены текста в custom label */
    customLabelTextReplace: '{data-template-quiz-label-text}',
  }

  /**
   *
   * @param {string|element} selector - Получаем готовый DOM элемент или строку для поиска всех элементов
   * @param {object} options - Получаем настройки для квиза
   * @returns {void}
   */
  constructor(selector, options = {}) {
    this.$el = null;
    this.$options = Object.assign(this.defaultOptions, options);
    if (!!selector && typeof selector === 'string') this.$el = document.querySelectorAll(selector)
    else if (!!selector && typeof selector !== 'string') this.$el = selector;

    if (!this.$el) return console.error('[Quiz]: Ошибка при получении элементов!')

    if (Array.prototype.slice.call(this.$el).length !== 0) {
      // Если получили NodeList
      if (this.$options.defaultTemplate) {
        // Если выбрали дефолтный шаблон
        this.$el.forEach(el => this.render(el, this.$options.questions));
      } else {
        // Если выбрали готовую разметку в html
        this.$el.forEach(el => this.remark(el));
      }
    } else {
      // Если один элемент
      if (this.$options.defaultTemplate) {
        // Если выбрали дефолтный шаблон
        this.render(this.$el, this.$options.questions);
      } else {
        // Если выбрали готовую разметку в html
        this.remark(this.$el);
      }
    }
  }

  /**
   * Получаем кастомный элемент label
   * @returns {HTMLElement}
   */
  #getTemplate() {
    if (!this.$options.customLabel) return false;
    const template = document.getElementById(this.$options.customLabel)
    if (!template) return false;
    const templateContent = template.content.firstElementChild

    return templateContent
  }

  /**
   *
   * @param {HTMLElement} selector - Получаем DOM элемент для отрисовки вопросов
   * @param {Array<object>} questions - Получаем массив с вариантами вопросов и ответов
   */
  render(selector, questions) {
    if (!selector) return console.error('[Quiz]: Ошибка при получении элемента!');
    if (!questions) return console.error('[Quiz]: Ошибка при получении вопросов!');

    const templateLabel = this.#getTemplate() // Получаем шаблон для прорисовки

    let userAnswers = [],
      oldIndex = 0,
      currentIndex = 1

    /**
     * Рендер состояния квиза
     * @param {Array<object>} arr - Получаем массив с вариантами вопросов и ответов
     * @returns {Promise<any>}
     */
    const quizController = (arr) => {
      selector.innerHTML = '';

      let promise = new Promise((resolve, reject) => {
        if (Array.isArray(arr)) {
          arr.forEach(el => {
            selector.innerHTML = `
                <h2>${this.$options.title}</h2>
                <p>${this.$options.desc}</p>
                <div class="quiz__wrapper"></div>
              `;
            const wrapper = document.querySelector('.quiz__wrapper')

            wrapper.insertAdjacentHTML('beforeend', `<h4 ${this.$options.questionNameAttr}>${el.title}</h4>`)
            el.answer.filter(answer => {
              if (templateLabel) {
                selector.insertAdjacentHTML('beforeend',
                  templateLabel.outerHTML
                    .replace(this.$options.customLabelReplace, `${this.$options.answerAttr} ${this.$options.answerDependentAttr}='${!!answer.questions ? JSON.stringify(answer.questions) : ""}' data-value="${answer.name}"`)
                    .replace(this.$options.customLabelTextReplace, answer.name)
                )
              } else {
                selector.insertAdjacentHTML(
                  'beforeend',

                  `<label ${this.$options.answerAttr} ${this.$options.answerDependentAttr}='${!!answer.questions ? JSON.stringify(answer.questions) : ""}' data-value="${answer.name}">${answer.name}</label>`)
              }
            });

            selector.querySelectorAll(this.$options.answerSel).forEach(label => {
              label.addEventListener('click', (e) => {
                currentIndex++
                oldIndex = currentIndex - 1

                const current = e.target.closest(this.$options.answerSel),
                  attrVal = current.getAttribute(this.$options.answerDependentAttr),
                  value = current.getAttribute('data-value'),
                  questionName = selector.querySelector(this.$options.questionNameSel);

                let name = null;
                if (questionName) name = questionName.innerText
                else console.error(`[Quiz]: Отсутствует имя ${this.$options.questionNameSel} для вопроса`);

                const isNextQuestions = attrVal ? JSON.parse(attrVal) : null,
                  arrNextQuestions = Array.isArray(isNextQuestions) ? isNextQuestions : {}

                userAnswers.push({ name, value });

                quizController(arrNextQuestions)

                if (Array.isArray(isNextQuestions)) quizBack(Array.isArray(arr) ? arr : {}, selector)

                if (this.$options.onChange !== undefined) this.$options.onChange(oldIndex, currentIndex, selector)
              })
            })
          })
        } else {
          // Формируем новый массив уникальных обе
          let obj = {},
            new_arr = []
          userAnswers.forEach((item) => {
            obj[item['name']] = item;
          });
          new_arr = Object.keys(obj).map((id) => {
            return obj[id];
          });

          return resolve(new_arr)
        }
      })

      /**
       * @promise {Array<object>} arr - Возвращаем массив с вариантами ответов в метод `onComplete`
       * @rejects {Error} err - Если что-то пошло не так, то возвращаем ошибку в метод `onError`
       */
      return promise.then((arr) => {
        if (this.$options.onComplete !== undefined) this.$options.onComplete(selector, arr, questions.length)
      }).catch((err) => {
        if (this.$options.onError !== undefined) this.$options.onError(selector, err)
      })
    }

    /**
     * Шаг назад
     * @param {Array<object>} arr - Получаем массив с объектами перед тем как показать следующие шаги
     * @param {HTMLElement} element - Получаем элемент для прорисовки кнопки
     */
    const quizBack = (arr, element) => {
      if (!Array.isArray(arr)) return false
      if (!element) return console.error('[Quiz]: Ошибка при получении элемента!');

      element.insertAdjacentHTML('beforeend', `
        <button type="button" data-quiz-back>Назад</button>
      `)

      currentIndex--
      oldIndex = currentIndex - 1

      const btn = element.querySelector('[data-quiz-back]')
      btn.addEventListener('click', () => quizController(arr))
    }

    quizController(questions)
  }

  /**
   * вариант квиза, когда разметка сгенерирована на странице
   * @param {element} selector - Получаем DOM элемент для поиска всех элементов
   */
  remark(selector) {
    if (!selector) return console.error('[Quiz]: Ошибка при получении элемента!');

    let answersStorage = {}, /* Используется для сохранения ответов при перезаписи зависимых ответов с предыдущего шага */
      userAnswers = [],
      currentActiveQuestion = 0;
    const questions = selector.querySelectorAll(this.$options.questionSel);

    const templateLabel = this.#getTemplate() // Получаем шаблон для прорисовки

    let promise = new Promise((resolve, reject) => {
      const questionBackButtons = selector.querySelectorAll(this.$options.backButtonSel);

      /**
       * Переключает отображение кнопок "Назад"
       * @param show - true скрывает кнопки, false отображает
       */
      const toggleBackButtons = show => {
        if (show) {
          questionBackButtons.forEach(button => {
            button.style.removeProperty('display');
          });
        } else {
          questionBackButtons.forEach(button => {
            button.style.setProperty('display', 'none');
          });
        }
      }


      /**
       * Устанавливает активный вопрос
       * @param {number} oldIndex - индекс предыдущего вопроса
       * @param {number} newIndex - индекс следующего вопроса
       */
      const setQuestion = (oldIndex, newIndex) => {
        if (oldIndex >= 0) questions[oldIndex].classList.remove('active');
        questions[newIndex].classList.add('active');
        toggleBackButtons(newIndex > 0);
        currentActiveQuestion = newIndex;
      }

      /**
       * производит рендер ответов для зависимого квиза. Перед этим записывает старые вопросы в answersStorage
       * @param {string} answersString - принимает строку с вопросами разделенными «|»
       * @param {number} questionIndex - индекс вопроса, в который нужно проставить новые ответы
       */
      const renderAnswers = (answersString, questionIndex) => {
        const newAnswers = answersString.split('|');

        const currentQuestion = questions[questionIndex];
        const answersContainer = currentQuestion.querySelector(this.$options.answersContainerSel);
        answersStorage[questionIndex] = answersContainer.innerHTML;
        answersContainer.innerHTML = '';
        newAnswers.forEach(answer => {
          if (!answer) return;
          if (templateLabel) {
            let templateLabelClean = templateLabel.outerHTML
              .replace(this.$options.customLabelReplace, `${this.$options.answerAttr} data-value='${answer}'`)
              .replace(this.$options.customLabelTextReplace, answer)
            ;
            answersContainer.insertAdjacentHTML('beforeend', templateLabelClean);
          } else {
            answersContainer.insertAdjacentHTML(
              'beforeend',
              `<label ${this.$options.answerAttr} data-value="${answer}">${answer}</label>`
            );
          }
        });

        const answers = currentQuestion.querySelectorAll(this.$options.answerSel);
        answers.forEach(answer => {
          if (answer) setClickHandlers(answer); // проверка на пустой элемент
        });
      }

      /**
       * вешает обработчик событий клика на вопрос
       * @param {HTMLElement} answer - label для которого нужно повесить обработчик клика
       */
      const setClickHandlers = (answer) => {
        answer.addEventListener('click', (e) => {
          e.preventDefault()
          //<editor-fold desc="Получение и записать name, value">
          const question = answer.closest(this.$options.questionSel);
          const questionName = question.querySelector(this.$options.questionNameSel);
          let name;
          if (questionName) {
            name = questionName.innerText;
          } else return console.error(`[Quiz]: Отсутствует имя ${this.$options.questionNameSel} для вопроса`, question);

          const value = answer.getAttribute('data-value');
          if (value) {
            question.querySelectorAll(`[${this.$options.answerAttr}]`).forEach(el => el.classList.remove('selected'))
            answer.classList.add('selected')

            if (userAnswers.some(el => el.name === name)) {
              userAnswers.filter(el => el.name === name ? el.value = value : null)
            } else {
              userAnswers.push({ name, value });
            }
          } else return console.error('[Quiz]: Ошибка при получении data-value элемента!', answer);
          //</editor-fold>


          if (userAnswers.length >= questions.length) {
            return resolve(userAnswers);
          }

          if (currentActiveQuestion + 1 != userAnswers.length) return false;

          const currentIndex = currentActiveQuestion;
          // let nextIndex = ++currentActiveQuestion;
          let nextIndex = currentActiveQuestion + 1;

          const changeStep = () => {
            const dependentAnswers = answer.getAttribute(this.$options.answerDependentAttr);
            if (dependentAnswers) renderAnswers(dependentAnswers, nextIndex);

            setQuestion(currentIndex, nextIndex);

            if (this.$options.onChange !== undefined) {
              this.$options.onChange(currentIndex, nextIndex, questions.length, selector)
            }
          }

          changeStep();
        });
      }

      /**
       * события для кнопки "Назад"
       * @param {HTMLElement} button - кнопка назад, на которую нужно повесить обработчик клика
       */
      const setBackButtonHandlers = (button) => {
        button.addEventListener('click', () => {
          if (currentActiveQuestion <= 0) return;

          const currentIndex = currentActiveQuestion;
          const nextIndex = --currentActiveQuestion;


          if (answersStorage.hasOwnProperty(currentIndex)) {
            const container = questions[currentIndex].querySelector(this.$options.answersContainerSel);
            if (container) {
              container.innerHTML = answersStorage[currentIndex];
              delete answersStorage[currentIndex];

              const questionAnswers = questions[currentIndex].querySelectorAll(this.$options.answerSel);
              questionAnswers.forEach(answer => setClickHandlers(answer));
            }
          }

          userAnswers.pop();
          setQuestion(currentIndex, nextIndex);

          if (this.$options.onChange !== undefined) {
            this.$options.onChange(currentIndex, nextIndex, questions.length, selector)
          }
        })
      }

      questions.forEach((question) => {
        const questionAnswers = question.querySelectorAll(this.$options.answerSel);
        questionAnswers.forEach(answer => setClickHandlers(answer));
      });

      questionBackButtons.forEach(button => setBackButtonHandlers(button));

      setQuestion(currentActiveQuestion - 1, currentActiveQuestion);
    });

    if (this.$options.onStart !== undefined) this.$options.onStart(selector, questions.length)

    return promise.then((arr) => {
      if (this.$options.onComplete !== undefined) this.$options.onComplete(selector, arr, questions.length)
    }).catch((err) => {
      if (this.$options.onError !== undefined) this.$options.onError(selector, err)
    })
  }
}

export default Quiz
