# Quiz

Lightweight JavaScript quiz library

### Features
-[x] Custom next step render
-[x] Independent answers
-[ ] Multiple choice

## Installation and Usage

Install the library with `npm install @southmedia/quiz` or  `yarn @southmedia/quiz`

#### ES6

```javascript
import Quiz from "@southmedia/quiz"
```

## Example

#### With use custom template:

```javascript
new Quiz('[data-quiz-inline]', {
  defaultTemplate: false, // For disabled usage default template
  customLabel: 'template-quiz-answer', // This point with usage custom render template for answers ` id template `
  onStart: (selector, length) => {
    /**
     * selector - parent HTMLElement
     * length - quantity questions
     */
  },
  onChange: (oldIndex, newIndex, length, selector) => {
    /**
     * oldIndex - previos index
     * newIndex - current index
     * length - quantity questions
     * selector - parent HTMLElement
     */
  },
  onComplete: (selector, arr, length) => {
    /**
     * selector - parent HTMLElement
     * arr - selected answers
     * length - quantity questions
     */
  },
  onError: (selector, err) => {
    /**
     * selector - parent HTMLElement
     * err - error message
     */
  }
})
```

#### With use html template
```html
<div class="quiz" data-quiz-inline>
    <div class="quiz__step" data-quiz-question>
        <h3 data-quiz-question-name>Question 1</h3>
        <div data-quiz-answers-container>
            <label data-quiz-answer data-value="Answer 1"
                   data-quiz-dependent="Answer for next question 1|"
            >Answer 1</label>
            <label data-quiz-answer data-value="Answer 2"
                   data-quiz-dependent="Answer for next question 1|Answer for next question 2"
            >Answer 2</label>
        </div>
        <div data-quiz-back>Back</div>
    </div>
    <div class="quiz__step" data-quiz-question>
        <h3 data-quiz-question-name>Question 2</h3>
        <div data-quiz-answers-container><!-- dependent questions from the first step will be rendered here --></div>
        <div data-quiz-back>Back</div>
    </div>
    <div class="quiz__step" data-quiz-question>
        <h3 data-quiz-question-name>Question 3</h3>
        <div data-quiz-answers-container>
            <label data-quiz-answer data-value="Answer 1">Answer 1</label>
            <label data-quiz-answer data-value="Answer 2">Answer 2</label>
        </div>
        <div data-quiz-back>Back</div>
    </div>
</div>
```

If template for [data-quiz-answer] differ from the default, then you need to add a new template to the page

```html
<template id="template-quiz-answer">
    <label class="quiz-label quiz-label--custom" {data-template-quiz-label}>
        <span class="quiz-label__box"></span>
        <span class="quiz-label__text">{data-template-quiz-label-text}</span>
    </label>
</template>
```

#### With use default template

> Ability to use dynamic questions/answers

```javascript
new Quiz('[data-quiz]', {
  customLabel: 'template-quiz-answer', // This point with usage custom render template for answers ` id template `
  title: "Title Quiz",
  desc: "Lorem ipsum",
  questions: [
    {
      title: "Inside which HTML element do we put the JavaScript?",
      answer: [
        {
          name: "<script>",
          questions: [
            {
              title: "Where is the correct place to insert a JavaScript?",
              answer: [
                {
                  name: "The <head> section"
                },
                {
                  name: "The <body> section"
                },
                {
                  name: "Both the <head> section and the <body> section are correct"
                }
              ]
            }
          ]
        },
        {
          name: "<javascript>",
          questions: [
            {
              title: "What is the correct syntax for referring to an external script called 'xxx.js'?",
              answer: [
                {
                  name: "<script href='xxx.js'>"
                },
                {
                  name: "<script name='xxx.js'>"
                },
                {
                  name: "<script src='xxx.js'>"
                }
              ]
            }
          ]
        },
        {
          name: "<scripting>",
          questions: [
            {
              title: "The external JavaScript file must contain the <script> tag.",
              answer: [
                {
                  name: "True"
                },
                {
                  name: "False"
                }
              ]
            }
          ]
        },
        {
          name: "<js>",
          questions: [
            {
              title: "How do you write 'Hello World' in an alert box?",
              answer: [
                {
                  name: "alert(Hello World)"
                },
                {
                  name: "alertBox('Hello World');"
                },
                {
                  name: "msg('Hello World');"
                },
                {
                  name: "alert('Hello World')"
                },
                {
                  name: "msgBox('Hello World')"
                }
              ]
            }
          ]
        },
      ]
    }
  ],
  onStart: (selector, length) => {
    /**
     * selector - parent HTMLElement
     * length - quantity questions
     */
  },
  onChange: (oldIndex, newIndex, length, selector) => {
    /**
     * oldIndex - previos index
     * newIndex - current index
     * length - quantity questions
     * selector - parent HTMLElement
     */
  },
  onComplete: (selector, arr, length) => {
    /**
     * selector - parent HTMLElement
     * arr - selected answers
     * length - quantity questions
     */
  },
  onError: (selector, err) => {
    /**
     * selector - parent HTMLElement
     * err - error message
     */
  }
})
```


## Maintainers

* [mayor](https://git.dev.southmedia.ru/mayor) - Victor Mayorov
* [achoplyak](https://git.dev.southmedia.ru/achoplyak) - Andrey Choplyak
